package pl.mzapisek.bikreport.report;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import pl.mzapisek.bikreport.person.Person;
import pl.mzapisek.bikreport.transaction.Transaction;

import java.math.BigDecimal;
import java.util.Collection;

@Builder
@Getter
@ToString
public class Report {

    @JsonProperty("person")
    private Person person;
    @JsonProperty("transactions")
    private Collection<Transaction> transactions;
    @JsonProperty("transactionsCount")
    private int transactionsCount;
    @JsonProperty("score")
    private int score;
    @JsonProperty("transactionSum")
    private BigDecimal transactionsSum;

}
