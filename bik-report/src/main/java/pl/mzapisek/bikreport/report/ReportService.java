package pl.mzapisek.bikreport.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mzapisek.bikreport.person.Person;
import pl.mzapisek.bikreport.transaction.Transaction;
import pl.mzapisek.bikreport.transaction.TransactionType;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.UUID;

// Currently its a fake service
@Service
public class ReportService {

    // TODO
    private final BikClient bikClient;

    @Autowired
    public ReportService(BikClient bikClient) {
        this.bikClient = bikClient;
    }

    // TODO report should be created by pesel, currently its just a stub
    public Report createReport(Person person) {
        // generated fake data, BikClient should be used instead
        final var transactions = Arrays.asList(
                new Transaction(UUID.randomUUID(), person, TransactionType.CREDIT, BigDecimal.valueOf(123)),
                new Transaction(UUID.randomUUID(), person, TransactionType.CREDIT_CARD_PAYMENT, BigDecimal.valueOf(777)),
                new Transaction(UUID.randomUUID(), person, TransactionType.DEBIT_CARD_PAYMENT, BigDecimal.valueOf(20)));

        return Report.builder()
                .person(person)
                .transactions(transactions)
                .transactionsCount(transactions.size())
                .transactionsSum(transactions.stream().map(Transaction::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add))
                .score(77)
                .build();
    }
}
