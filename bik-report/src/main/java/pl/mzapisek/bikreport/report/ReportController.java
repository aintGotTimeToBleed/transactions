package pl.mzapisek.bikreport.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mzapisek.bikreport.person.Person;

@RestController
@RequestMapping("/reports")
public class ReportController {

    private final ReportService reportService;

    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    // TODO should be GET and by pesel not person
    @PostMapping("/person")
    public ResponseEntity<Report> findReportByPerson(@RequestBody Person person) {
        // TODO - person validator
        return new ResponseEntity<>(reportService.createReport(person), HttpStatus.OK);
    }
}
