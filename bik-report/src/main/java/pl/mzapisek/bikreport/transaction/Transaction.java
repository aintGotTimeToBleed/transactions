package pl.mzapisek.bikreport.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import pl.mzapisek.bikreport.person.Person;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Transaction {

    @JsonProperty("id")
    private UUID id;
    @JsonProperty("person")
    private Person person;
    @JsonProperty("transactionType")
    private TransactionType transactionType;
    @JsonProperty("amount")
    private BigDecimal amount;

}
