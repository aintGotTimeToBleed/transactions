# Transaction Verifier

## Starting application
* ```docker-compose up -d```
* start producer
* start bik-rest (at that moment actually its not used)
* start bik-report (this service should have logic for calculating bik score and call bik-rest, but its not implemented yet)
* start verifier
* start consumer (consumer should call verifier, but its not implemented yet)

### TODO
* consumer should call verifier
* add mongo to verifier
* make verifier really reactive
* additional tests needed in verifier
* upgrade bik-report to use bik-rest
* complete todos added to code
* add resilience4j when calling bik-report
* add distributed tracing
* add service discovery
