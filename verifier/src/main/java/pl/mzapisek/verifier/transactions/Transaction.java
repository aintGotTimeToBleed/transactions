package pl.mzapisek.verifier.transactions;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import pl.mzapisek.verifier.person.Person;

import java.math.BigDecimal;
import java.util.UUID;

@AllArgsConstructor
@ToString
@Getter
public class Transaction {

    @JsonProperty
    private UUID id;
    @JsonProperty("person")
    private Person person;
    @JsonProperty("transactionType")
    private TransactionType transactionType;
    @JsonProperty("amount")
    // TODO should not be null constraint
    private BigDecimal amount;

}

