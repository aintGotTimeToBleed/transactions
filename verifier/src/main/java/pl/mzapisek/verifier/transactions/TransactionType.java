package pl.mzapisek.verifier.transactions;

public enum TransactionType {
    CREDIT_CARD_PAYMENT, DEBIT_CARD_PAYMENT, CREDIT
}
