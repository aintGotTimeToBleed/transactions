package pl.mzapisek.verifier.engine.rules;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.mzapisek.verifier.transactions.Transaction;
import pl.mzapisek.verifier.transactions.TransactionType;

@Component
public class JonSnowCreditCardPayment implements Rule {

    @Value("${rule.jon-snow-credit-card-payment}")
    private Integer score;

    @Override
    public Integer getScore() {
        return score;
    }

    @Override
    public boolean test(Transaction transaction) {
        var personLastName = transaction.getPerson().getLastName();
        var transactionType = transaction.getTransactionType();

        return personLastName.toLowerCase().equals("snow") && transactionType == TransactionType.CREDIT_CARD_PAYMENT;
    }
}
