package pl.mzapisek.verifier.engine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mzapisek.verifier.engine.rules.Rule;
import pl.mzapisek.verifier.transactions.Transaction;

import java.util.List;

@Service
public class ScoringEngine {

    private final List<Rule> rules;

    @Autowired
    public ScoringEngine(List<Rule> rules) {
        this.rules = rules;
    }

    public Integer getScoring(Transaction transaction) {
       return rules
                .stream()
                .filter(rule -> rule.test(transaction))
                .map(Rule::getScore)
                .reduce(Integer::sum)
                .orElse(0);
    }
}
