package pl.mzapisek.verifier.engine.rules;

import pl.mzapisek.verifier.transactions.Transaction;

public interface Rule {

    Integer getScore();

    boolean test(Transaction transaction);

}
