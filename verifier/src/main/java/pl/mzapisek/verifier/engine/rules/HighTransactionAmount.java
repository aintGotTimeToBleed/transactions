package pl.mzapisek.verifier.engine.rules;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.mzapisek.verifier.transactions.Transaction;

import java.math.BigDecimal;

@Component
public class HighTransactionAmount implements Rule {

    private static final BigDecimal AMOUNT = BigDecimal.valueOf(15000);

    @Value("${rule.high-transaction-amount}")
    private Integer score;

    @Override
    public Integer getScore() {
        return score;
    }

    @Override
    public boolean test(Transaction transaction) {
        return transaction.getAmount().compareTo(AMOUNT) == 1;
    }
}
