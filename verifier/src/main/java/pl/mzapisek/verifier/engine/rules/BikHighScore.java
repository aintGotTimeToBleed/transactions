package pl.mzapisek.verifier.engine.rules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.mzapisek.verifier.bik.BikClient;
import pl.mzapisek.verifier.bik.Report;
import pl.mzapisek.verifier.transactions.Transaction;

@Component
public class BikHighScore implements Rule {

    @Value("${rule.bik-high-score}")
    private Integer score;

    private static final Integer HIGH_SCORE = 50;

    private BikClient bikClient;

    @Autowired
    public BikHighScore(BikClient bikClient) {
        this.bikClient = bikClient;
    }

    @Override
    public Integer getScore() {
        return score;
    }

    @Override
    public boolean test(Transaction transaction) {
        Report report = bikClient.callBik(transaction.getPerson());

        return report.getScore() > HIGH_SCORE;
    }
}
