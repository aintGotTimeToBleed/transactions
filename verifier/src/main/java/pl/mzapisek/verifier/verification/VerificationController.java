package pl.mzapisek.verifier.verification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.mzapisek.verifier.transactions.Transaction;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@RequestMapping("/verifications")
public class VerificationController {

    private final VerificationService verificationService;

    @Autowired
    public VerificationController(VerificationService verificationService) {
        this.verificationService = verificationService;
    }

    @PostMapping("/transactions")
    public Mono<VerificationResult> postTransaction(@RequestBody Transaction transaction) {
        // TODO transaction validator, add @Valid

        return verificationService.verifyTransaction(transaction);
    }

    @GetMapping("/transactions")
    public Flux<VerificationResult> getTransactions() {
        // TODO, fake, should get data from reactive repository
        return Flux.just(new VerificationResult(UUID.randomUUID(), UUID.randomUUID(), 77));
    }
}
