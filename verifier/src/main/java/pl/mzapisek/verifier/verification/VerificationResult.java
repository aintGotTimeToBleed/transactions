package pl.mzapisek.verifier.verification;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.UUID;

@AllArgsConstructor
@Getter
@ToString
@Builder
public class VerificationResult {

    @JsonProperty("id")
    private UUID id;
    @JsonProperty("transactionId")
    private UUID transactionId;
    @JsonProperty("score")
    private Integer score;

}
