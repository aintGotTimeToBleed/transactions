package pl.mzapisek.verifier.verification;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.mzapisek.verifier.engine.ScoringEngine;
import pl.mzapisek.verifier.producer.Producer;
import pl.mzapisek.verifier.transactions.Transaction;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class VerificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerificationService.class.getName());

    private final Producer producer;
    private final ScoringEngine scoringEngine;

    public VerificationService(Producer producer, ScoringEngine scoringEngine) {
        this.producer = producer;
        this.scoringEngine = scoringEngine;
    }

    public Mono<VerificationResult> verifyTransaction(Transaction transaction) {
        // TODO add dlq for transactions which got errors
        var scoring = scoringEngine.getScoring(transaction);
        var verificationResult = new VerificationResult(UUID.randomUUID(), transaction.getId(), scoring);
        var objectMapper = new ObjectMapper();

        try {
            producer.send(transaction.getPerson().getPesel(), objectMapper.writeValueAsString(verificationResult));
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage());
        }

        return Mono.just(verificationResult);
    }
}
