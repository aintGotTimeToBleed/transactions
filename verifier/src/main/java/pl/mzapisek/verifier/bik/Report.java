package pl.mzapisek.verifier.bik;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.mzapisek.verifier.person.Person;
import pl.mzapisek.verifier.transactions.Transaction;

import java.math.BigDecimal;
import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
public class Report {

    private Person person;
    private Collection<Transaction> transactions;
    private int transactionsCount;
    private int score;
    private BigDecimal transactionsSum;

}
