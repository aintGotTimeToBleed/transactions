package pl.mzapisek.verifier.bik;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.mzapisek.verifier.person.Person;

@Service
public class BikClient {

    @Value("${bik.report.url}")
    private String bikReportUrl;

    private final RestTemplate restTemplate;

    @Autowired
    public BikClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Report callBik(Person person) {
        ResponseEntity<Report> reportResponseEntity = restTemplate.postForEntity(bikReportUrl, person, Report.class);
        return reportResponseEntity.getBody();
    }
}
