package pl.mzapisek.verifier;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import pl.mzapisek.verifier.person.Person;
import pl.mzapisek.verifier.transactions.Transaction;
import pl.mzapisek.verifier.transactions.TransactionType;
import pl.mzapisek.verifier.verification.VerificationResult;
import pl.mzapisek.verifier.verification.VerificationService;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.UUID;

import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@WebFluxTest
public class VerificationControllerTest {

    @Autowired
    WebTestClient webTestClient;

    @MockBean
    VerificationService verificationService;

    @Test
    public void postTransaction_ShouldReturnVerificationResult() {

        final var transaction = new Transaction(
                UUID.randomUUID(),
                new Person("Jon", "Snow", "12345"),
                TransactionType.CREDIT,
                BigDecimal.valueOf(777));

        final var verificationResult = VerificationResult
                .builder()
                .id(UUID.randomUUID())
                .transactionId(transaction.getId())
                .score(77)
                .build();

        when(verificationService.verifyTransaction(transaction)).thenReturn(Mono.just(verificationResult));

        webTestClient.post()
                .uri("/verifications/transactions")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(verificationResult), VerificationResult.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody(VerificationResult.class);
    }
}
