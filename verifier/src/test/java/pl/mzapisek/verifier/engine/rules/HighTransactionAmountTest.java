package pl.mzapisek.verifier.engine.rules;

import org.junit.Assert;
import org.junit.Test;
import pl.mzapisek.verifier.transactions.Transaction;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;

public class HighTransactionAmountTest {

    @Test
    public void shouldReturnTrue_WhenHighTransactionAmountIsPassed() {
        var transaction = mock(Transaction.class);
        when(transaction.getAmount()).thenReturn(BigDecimal.valueOf(50000));

        var highTransactionAmount = new HighTransactionAmount();
        Assert.assertTrue(highTransactionAmount.test(transaction));
    }

    @Test
    public void shouldReturnFalse_WhenLowTransactionAmountIsPassed() {
        var transaction = mock(Transaction.class);
        when(transaction.getAmount()).thenReturn(BigDecimal.valueOf(500));

        var highTransactionAmount = new HighTransactionAmount();
        Assert.assertFalse(highTransactionAmount.test(transaction));
    }
}
