package pl.mzapisek.verifier.engine.rules;

import org.junit.Assert;
import org.junit.Test;
import pl.mzapisek.verifier.bik.BikClient;
import pl.mzapisek.verifier.bik.Report;
import pl.mzapisek.verifier.person.Person;
import pl.mzapisek.verifier.transactions.Transaction;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BikHighScoreTest {

    @Test
    public void shouldReturnTrue() {
        var report = mock(Report.class);
        when(report.getScore()).thenReturn(77);
        var bikClient = mock(BikClient.class);
        when(bikClient.callBik(any())).thenReturn(report);

        var transaction = mock(Transaction.class);
        var person = mock(Person.class);
        when(transaction.getPerson()).thenReturn(person);

        var bikHighScore = new BikHighScore(bikClient);
        Assert.assertTrue(bikHighScore.test(transaction));
    }
}
