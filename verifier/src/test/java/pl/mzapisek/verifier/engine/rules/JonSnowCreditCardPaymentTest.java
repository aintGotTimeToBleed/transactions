package pl.mzapisek.verifier.engine.rules;

import org.junit.Assert;
import org.junit.Test;
import pl.mzapisek.verifier.person.Person;
import pl.mzapisek.verifier.transactions.Transaction;
import pl.mzapisek.verifier.transactions.TransactionType;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JonSnowCreditCardPaymentTest {

    @Test
    public void shouldReturnTrue() {
        var transaction = mock(Transaction.class);
        when(transaction.getTransactionType()).thenReturn(TransactionType.CREDIT_CARD_PAYMENT);

        var person = mock(Person.class);
        when(person.getLastName()).thenReturn("Snow");
        when(transaction.getPerson()).thenReturn(person);

        var jonSnowCreditCardPayment = new JonSnowCreditCardPayment();
        Assert.assertTrue(jonSnowCreditCardPayment.test(transaction));
    }

    @Test
    public void shouldReturnFalse() {
        var transaction = mock(Transaction.class);
        when(transaction.getTransactionType()).thenReturn(TransactionType.CREDIT);

        var person = mock(Person.class);
        when(person.getLastName()).thenReturn("Snow");
        when(transaction.getPerson()).thenReturn(person);

        var jonSnowCreditCardPayment = new JonSnowCreditCardPayment();
        Assert.assertFalse(jonSnowCreditCardPayment.test(transaction));
    }
}
