package pl.mzapisek.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.mzapisek.consumer.event.consumer.KafkaConsumerService;

@Component
public class ConsumerCommandLineRunner implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerCommandLineRunner.class.getName());
    private KafkaConsumerService kafkaConsumerService;

    @Autowired
    public ConsumerCommandLineRunner(KafkaConsumerService kafkaConsumerService) {
        this.kafkaConsumerService = kafkaConsumerService;
    }

    @Override
    public void run(String... args) throws Exception {
        LOGGER.debug("starting consumer");
        kafkaConsumerService.consume();
    }
}
