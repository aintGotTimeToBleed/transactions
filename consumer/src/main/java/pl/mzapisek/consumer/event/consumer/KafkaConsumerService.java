package pl.mzapisek.consumer.event.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

@Service
public class KafkaConsumerService {

    @Value("${kafka.bootstrap-servers}")
    private String bootstrapServers;
    @Value("${kafka.consumer.topic}")
    private String topic;
    @Value("${kafka.consumer.group-id}")
    private String groupId;
    @Value("${kafka.consumer.auto-offset-reset}")
    private String autoOffsetReset;

    final Properties consumerProperties = new Properties();
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumerService.class.getName());
    KafkaConsumer<String, String> consumer;

    public void consume() {
        while(true) {
            final var records = consumer.poll(Duration.ofMillis(100));
            records.forEach(record -> {
                final var objectMapper = new ObjectMapper();
                try {
                    final var root = objectMapper.readTree(record.value());

                    // TODO webclient, should call transaction verifier

                    LOGGER.info("Event consumed: " + root.path("id"));
                } catch (IOException e) {
                    LOGGER.error(e.getMessage());
                }
            });
        }
    }

    @PostConstruct
    public void createConsumer() {
        consumerProperties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        consumerProperties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerProperties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        consumerProperties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        consumerProperties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);

        consumer = new KafkaConsumer<>(consumerProperties);
        consumer.subscribe(Arrays.asList(topic));
    }

    @PreDestroy
    public void destroy() {
        LOGGER.debug("closing consumer");
        consumer.close();
    }
}
