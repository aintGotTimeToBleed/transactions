package pl.mzapisek.bikrest.person;

public class PersonNotFoundException extends RuntimeException {

    public PersonNotFoundException(Integer personId) {
        super("Person not found with id: " + personId);
    }
}
