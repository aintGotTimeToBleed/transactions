package pl.mzapisek.bikrest.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import pl.mzapisek.bikrest.score.Score;
import pl.mzapisek.bikrest.transaction.Transaction;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "person")
@Getter
@Setter
public class Person {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "pesel")
    private String pesel;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @Getter(onMethod = @__(@JsonIgnore))
    private Collection<Score> scores;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    @Getter(onMethod = @__(@JsonIgnore))
    private Collection<Transaction> transactions;
}
