package pl.mzapisek.bikrest.score;

public class ScoreNotFoundException extends RuntimeException {

    public ScoreNotFoundException() {
        super("No active score found for person");
    }

}
