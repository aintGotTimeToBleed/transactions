package pl.mzapisek.bikrest.score;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import pl.mzapisek.bikrest.person.PersonNotFoundException;

@RepositoryRestController
public class ScoreController {

    private final ScoreService scoreService;
    private final ScoreResourceAssembler scoreResourceAssembler;

    @Autowired
    public ScoreController(ScoreService scoreService, ScoreResourceAssembler scoreResourceAssembler) {
        this.scoreService = scoreService;
        this.scoreResourceAssembler = scoreResourceAssembler;
    }

    @PostMapping("/scores")
    public ResponseEntity<Resource<Score>> saveToPerson(@RequestBody Score score, @RequestParam Integer personId) throws PersonNotFoundException {
        final var savedScore = scoreService.saveToPerson(score, personId);

        return new ResponseEntity<>(scoreResourceAssembler.toResource(savedScore), HttpStatus.OK);
    }

    @GetMapping("/scores/active")
    public ResponseEntity<Resource<Score>> findPersonActiveScore(@RequestParam String pesel) throws ScoreNotFoundException {
        final var score = scoreService.findActiveScoreForPersonByPesel(pesel);

        return new ResponseEntity<>(scoreResourceAssembler.toResource(score), HttpStatus.OK);
    }
}
