package pl.mzapisek.bikrest.score;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mzapisek.bikrest.person.Person;
import pl.mzapisek.bikrest.person.PersonNotFoundException;
import pl.mzapisek.bikrest.person.PersonRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ScoreService {

    private final ScoreRepository scoreRepository;
    private final PersonRepository personRepository;

    @Autowired
    public ScoreService(ScoreRepository scoreRepository, PersonRepository personRepository) {
        this.scoreRepository = scoreRepository;
        this.personRepository = personRepository;
    }

    public Score saveToPerson(Score score, Integer personId) throws PersonNotFoundException {
        disablePersonPreviousScores(personId);
        Optional<Person> personOptional = personRepository.findById(personId);
        final var person = personOptional.orElseThrow(() -> new PersonNotFoundException(personId));
        score.setPerson(person);
        score.setActive(true);
        score.setActivatedAt(new Date());

        return scoreRepository.save(score);
    }

    public Score findActiveScoreForPersonByPesel(String pesel) {
        final var scoreOptional = scoreRepository.findByPersonPeselAndActiveTrue(pesel);
        return scoreOptional.orElseThrow(ScoreNotFoundException::new);
    }

    private void disablePersonPreviousScores(Integer personId) {
        List<Score> scores = scoreRepository.findByPersonId(personId);
        scores.forEach(score -> {
            score.setActive(false);
            score.setDisabledAt(new Date());
        });
        scoreRepository.saveAll(scores);
    }
}
