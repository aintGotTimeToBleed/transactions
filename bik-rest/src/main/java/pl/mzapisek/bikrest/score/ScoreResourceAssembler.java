package pl.mzapisek.bikrest.score;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class ScoreResourceAssembler implements ResourceAssembler<Score, Resource<Score>> {

    private RepositoryEntityLinks entityLinks;

    @Autowired
    public ScoreResourceAssembler(RepositoryEntityLinks entityLinks) {
        this.entityLinks = entityLinks;
    }

    @Override
    public Resource<Score> toResource(Score scoreEntity) {
        LinkBuilder linkBuilder = entityLinks.linkForSingleResource(ScoreRepository.class, scoreEntity.getId());
        Resource<Score> resource = new Resource<>(scoreEntity);
        resource.add(linkBuilder.withSelfRel());
        resource.add(linkBuilder.slash("person").withRel("person"));

        return resource;
    }
}
