package pl.mzapisek.bikrest.score;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import pl.mzapisek.bikrest.person.Person;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "score")
@Getter
@Setter
public class Score {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    @JoinColumn(name = "person_id", nullable = false)
    @ManyToOne
    @Setter(onMethod = @__(@JsonIgnore))
    @NotNull
    private Person person;

    @Column(name = "score")
    private Integer scoreNumeric;

    @Column(name = "active")
    private boolean active;

    @Column(name = "activated_at")
    private Date activatedAt;

    @Column(name = "disabled_at")
    private Date disabledAt;
}
