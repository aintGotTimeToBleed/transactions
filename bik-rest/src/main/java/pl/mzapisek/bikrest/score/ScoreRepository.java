package pl.mzapisek.bikrest.score;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface ScoreRepository extends PagingAndSortingRepository<Score, Integer> {

    Optional<Score> findByPersonPeselAndActiveTrue(String pesel);

    List<Score> findByPersonId(Integer personId);

}
