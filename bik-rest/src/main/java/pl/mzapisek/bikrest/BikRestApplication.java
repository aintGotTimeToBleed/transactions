package pl.mzapisek.bikrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BikRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(BikRestApplication.class, args);
    }

}
