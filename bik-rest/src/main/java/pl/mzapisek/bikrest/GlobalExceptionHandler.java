package pl.mzapisek.bikrest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import pl.mzapisek.bikrest.person.PersonNotFoundException;
import pl.mzapisek.bikrest.score.ScoreNotFoundException;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class.getName());

    @ExceptionHandler(DataAccessResourceFailureException.class)
    public final ResponseEntity<?> handleConnectionException(Exception ex, WebRequest webRequest) {
        LOGGER.error("exception message: " + ex.getMessage() + "; request: " + webRequest.getContextPath());

        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({PersonNotFoundException.class, ScoreNotFoundException.class})
    public final ResponseEntity<?> handleNotFoundException(Exception ex, WebRequest webRequest) {
        LOGGER.error("exception message: " + ex.getMessage() + "; request: " + webRequest.getContextPath());

        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }
}
