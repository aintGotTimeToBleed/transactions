package pl.mzapisek.bikrest.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import pl.mzapisek.bikrest.person.Person;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "transaction")
public class Transaction {

    private static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "person_id", nullable = false)
    @NotNull
    private Person person;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    @JsonProperty("transactionType")
    private TransactionType type;

    @Column(name = "amount")
    private BigDecimal amount;
}
