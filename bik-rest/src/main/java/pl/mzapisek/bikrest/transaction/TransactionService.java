package pl.mzapisek.bikrest.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mzapisek.bikrest.person.Person;
import pl.mzapisek.bikrest.person.PersonNotFoundException;
import pl.mzapisek.bikrest.person.PersonRepository;

import java.util.Optional;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final PersonRepository personRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository, PersonRepository personRepository) {
        this.transactionRepository = transactionRepository;
        this.personRepository = personRepository;
    }

    public Transaction saveToPerson(Transaction transaction, Integer personId) throws PersonNotFoundException {
        Optional<Person> optPerson = personRepository.findById(personId);
        Person person = optPerson.orElseThrow(() -> new PersonNotFoundException(personId));
        transaction.setPerson(person);

        return transactionRepository.save(transaction);
    }
}
