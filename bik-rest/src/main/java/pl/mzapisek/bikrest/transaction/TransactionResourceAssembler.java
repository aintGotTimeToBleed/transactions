package pl.mzapisek.bikrest.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class TransactionResourceAssembler implements ResourceAssembler<Transaction, Resource<Transaction>> {

    private RepositoryEntityLinks entityLinks;

    @Autowired
    public TransactionResourceAssembler(RepositoryEntityLinks entityLinks) {
        this.entityLinks = entityLinks;
    }

    @Override
    public Resource<Transaction> toResource(Transaction transactionEntity) {
        final var linkBuilder = entityLinks.linkForSingleResource(TransactionRepository.class, transactionEntity.getId());
        final var transactionResource = new Resource<>(transactionEntity);
        transactionResource.add(linkBuilder.withSelfRel());
        transactionResource.add(linkBuilder.slash("person").withRel("person"));
        return null;
    }
}
