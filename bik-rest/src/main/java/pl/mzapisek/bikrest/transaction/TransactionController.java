package pl.mzapisek.bikrest.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@RepositoryRestController
public class TransactionController {

    private final TransactionService transactionService;
    private final TransactionResourceAssembler transactionResourceAssembler;

    @Autowired
    public TransactionController(TransactionService transactionService, TransactionResourceAssembler transactionResourceAssembler) {
        this.transactionService = transactionService;
        this.transactionResourceAssembler = transactionResourceAssembler;
    }

    @PostMapping("/transactions")
    public ResponseEntity<Resource<Transaction>> saveToPerson(@RequestBody Transaction transaction, @RequestParam Integer personId) {
        var savedTransaction = transactionService.saveToPerson(transaction, personId);

        return new ResponseEntity<>(transactionResourceAssembler.toResource(savedTransaction), HttpStatus.OK);
    }
}
