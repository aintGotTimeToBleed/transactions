package pl.mzapisek.bikrest.transaction;

public enum TransactionType {
    CREDIT_CARD_PAYMENT, DEBIT_CARD_PAYMENT, CREDIT
}
