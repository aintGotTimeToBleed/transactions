package pl.mzapisek.transaction.producer.event.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Properties;

// TODO rethink, producer as bean, then producer could be easily changed
@Service
public class KafkaProducerImpl implements Producer {

    @Value("${kafka.bootstrap-servers}")
    private String bootstrapServer;
    @Value("${kafka.topic}")
    private String topic;

    private KafkaProducer<String, String> producer;

    public void send(String key, String value) {
        // TODO add callback for logging event sent action or exception throwing, maybe zipkin for distributed tracing
        producer.send(new ProducerRecord<>(topic, key, value));
    }

    @PostConstruct
    public void init() {
        final var properties = new Properties();

        // TODO rethink properties, producer should be more safe
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        producer = new KafkaProducer<>(properties);
    }

    @PreDestroy
    private void preDestroy() {
        producer.close();
    }
}
