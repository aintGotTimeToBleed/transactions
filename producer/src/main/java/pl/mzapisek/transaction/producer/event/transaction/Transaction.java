package pl.mzapisek.transaction.producer.event.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import pl.mzapisek.transaction.producer.person.Person;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@ToString
@AllArgsConstructor
public class Transaction {

    @JsonProperty("id")
    private UUID id;
    @JsonProperty("person")
    private Person person;
    @JsonProperty("amount")
    private BigDecimal amount;
    @JsonProperty("transactionType")
    private TransactionType type;

}
