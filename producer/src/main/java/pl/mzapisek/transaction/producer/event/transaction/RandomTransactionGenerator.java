package pl.mzapisek.transaction.producer.event.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mzapisek.transaction.producer.person.PersonRepository;

import java.math.BigDecimal;
import java.util.Random;
import java.util.UUID;

@Service
public class RandomTransactionGenerator {

    private final int MAX_TRANSACTION_AMOUNT = 10000;

    private PersonRepository personRepository;

    @Autowired
    public RandomTransactionGenerator(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Transaction generateRandomTransaction() {
        final var allPerson = personRepository.findAll();
        final var person = allPerson.stream()
                .skip(new Random().nextInt(allPerson.size() - 1))
                .findFirst()
                .get();

        return new Transaction(UUID.randomUUID(), person, generateRandomAmount(), generateRandomTransactionType());
    }

    private BigDecimal generateRandomAmount() {
        return BigDecimal.valueOf(new Random().nextInt(MAX_TRANSACTION_AMOUNT));
    }

    private TransactionType generateRandomTransactionType() {
        return TransactionType.values()[new Random().nextInt(TransactionType.values().length)];
    }
}
