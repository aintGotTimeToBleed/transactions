package pl.mzapisek.transaction.producer.event.transaction;

public enum TransactionType {
    CREDIT_CARD_PAYMENT, DEBIT_CARD_PAYMENT, CREDIT
}
