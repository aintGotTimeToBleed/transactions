package pl.mzapisek.transaction.producer.event.producer;

public interface Producer {
    void send(String key, String value);
}
