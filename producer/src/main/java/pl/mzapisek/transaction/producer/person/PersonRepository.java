package pl.mzapisek.transaction.producer.person;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PersonRepository {

    private List<Person> personList = new ArrayList<>();

    public List<Person> findAll() {
        return personList;
    }

    {
        personList.add(new Person("Jon", "Snow", "92070215387"));
        personList.add(new Person("Daenerys", "Targaryen", "89073159536"));
        personList.add(new Person("Arya", "Stark", "73100665217"));
        personList.add(new Person("Sansa", "Stark", "65051312863"));
        personList.add(new Person("Tyrion", "Lannister", "56073162985"));
    }
}
