package pl.mzapisek.transaction.producer.event.transaction;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.mzapisek.transaction.producer.event.producer.Producer;

@Component
public class TransactionEmitter {

    private Producer producer;
    private RandomTransactionGenerator randomTransactionGenerator;

    @Autowired
    public TransactionEmitter(Producer producer, RandomTransactionGenerator randomTransactionGenerator) {
        this.producer = producer;
        this.randomTransactionGenerator = randomTransactionGenerator;
    }

    @Scheduled(initialDelay = 2000, fixedRate = 2000)
    public void generateEvent() throws JsonProcessingException {
        final var objectMapper = new ObjectMapper();
        final var transaction = randomTransactionGenerator.generateRandomTransaction();

        producer.send(transaction.getPerson().getPesel(), objectMapper.writeValueAsString(transaction));
    }
}
